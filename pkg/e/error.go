package e

type ApiError struct {
	Status int    `json:"-"`
	Code   int    `json:"code"`
	Msg    string `json:"msg"`
}

func (err ApiError) Error() string {
	return err.Msg
}
