package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"sync"
)

type Ab []struct {
}
type inter int

type Set struct {
	m map[inter]int
	sync.RWMutex
}

func New() *Set {
	return &Set{
		m: map[inter]int{},
	}
}

func (s *Set) Add(item inter) {
	s.Lock()
	defer s.Unlock()
	s.m[item] = 2
}

var (
	res = gin.H{}
)

var arr = make([]string, 4, 4)

func Testa(c *gin.Context) {
	arr[0] = "hello"
	fmt.Print("%d", arr)
	fmt.Print("%+v", arr)
	fmt.Print("%s", arr)
	//wg:= sync.WaitGroup{}
	//wg.Add(2)
	//go func() {
	//	wg.Done()
	//}()
	//go func() {
	//	wg.Done()
	//}()
	//wg.Wait()
	//aaa := map[string]int{}
	//bbb := make(map[string]int)
	//aaa["aa"] = 5
	//bbb["bb"] = 7
	//fmt.Println(aaa,bbb)

	//runtime.GOMAXPROCS(1)
	//wg := sync.WaitGroup{}
	//wg.Add(200)
	//
	//for i := 0; i < 100; i++ {
	//	go func() {
	//		fmt.Println("a=", i)
	//	}()
	//	wg.Done()
	//}
	//for i := 0; i < 100; i++ {
	//	go func(i int) {
	//		fmt.Println("b=", i)
	//	}(i)
	//	wg.Done()
	//}

	//arr := make([]interface{}, 3)
	//arr[0] = 1
	//arr[1] = 2
	//arr[2] = 3
	//arr = append(arr, 222)
	//arr = append(arr, "111")
	//arr = append(arr, 123)
	//for _, v := range arr {
	//	if val, ok := v.(int)
	//	ok == true{
	//
	//	}
	//}
	//res["data"] = arr
	//writeJSON(c, res)
	//r := New()
	//r.Add(4)
	//r.Add(6)
	//fmt.Println(Set{})
	//fmt.Println(r.m)
}
