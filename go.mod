module example.com/m/v2

go 1.15

require (
	github.com/Shopify/sarama v1.27.2
	github.com/astaxie/beego v1.12.3 // indirect
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/garyburd/redigo v1.6.2
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ini/ini v1.62.0 // indirect
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
