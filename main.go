package main

import (
	"example.com/m/v2/models"
	"example.com/m/v2/routers"
	"fmt"
	"log"
	"net/http"
)

//定义User类型结构

func main() {

	db, err := models.InitDB()
	if err != nil {
		log.Print("err open databases", err)
		return
	}
	defer db.Close()

	router := routers.InitRouter()

	s := &http.Server{
		Addr:    fmt.Sprintf(":%d", 9000),
		Handler: router,
		//ReadTimeout:    setting.ReadTimeout,
		//WriteTimeout:   setting.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}
	s.ListenAndServe()

}
