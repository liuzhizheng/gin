package routers

import (
	"example.com/m/v2/controllers"
	"example.com/m/v2/helper"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.POST("/test", helper.Tes(), controllers.Test)
	r.GET("/tests", controllers.Addblog)
	r.GET("/abc", controllers.Abc)
	r.GET("/test", controllers.Tests)
	r.GET("/testa", controllers.Testa)
	r.GET("/producer", controllers.Producer)
	r.GET("/consumer", controllers.Consumer)
	r.Run()
	return r
}
